import {InputLabel} from "@mui/material";
import React, {useEffect} from "react";

type ObjectInputProps = {
    id?: string;
    value: string;
    updateValue: (newValue: string) => void;
};

const ObjectInput = (props: ObjectInputProps) => {

    const [inputValue, setInputValue] = React.useState(JSON.stringify(props.value));

    useEffect(() => {
        props.updateValue(inputValue);
    }, [inputValue]);

    return (
        <>
            <div id='#/properties/rating' className='rating'>
                <input value={inputValue} onChange={(e) => setInputValue(e.target.value)}/>
            </div>
        </>
    );
}
export default ObjectInput;
