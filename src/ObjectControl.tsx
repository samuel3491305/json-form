import { withJsonFormsControlProps } from '@jsonforms/react';
import ObjectInput from "./ObjectInput";
import React from "react";
import {InputLabel} from "@mui/material";

interface ObjectControlProps {
    data: any;
    handleChange(path: string, value: any): void;
    path: string;
    id: string;
    label: string;
}

const ObjectControl = (props: ObjectControlProps) => {
    const { label, data, handleChange, path } = props;
    return (
        <>
            <InputLabel shrink style={{ marginTop: '0.8em' }}>{label}</InputLabel>
            <ObjectInput value={data} updateValue={(val) => handleChange(path, val)} />
        </>
    );
};

export default withJsonFormsControlProps(ObjectControl);
