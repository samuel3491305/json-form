import {or, rankWith, scopeEndsWith} from '@jsonforms/core';
// import {and, schemaTypeIs, uiTypeIs} from "@jsonforms/core/src/testers/testers";

export default rankWith(
    3, //increase rank as needed
    or(scopeEndsWith('Map'), scopeEndsWith('Roles'))
);
